##ScrabbleScript_

  #Scrabble Script console:

  *After download/unpack to yours local location folder, If You want to launch program write in bash console- "python3.* run.py";

  ![](https://bytebucket.org/Rklab3/scrabblescript_/raw/24ca8e12d446243ac193b98a88659591eb523717/img/scrabble_.png)

  > Program after launched, create the file dictionary.txt - default empty. You may paste all Your words to dictionary.txt file by example(in vertically convention): "word\n anotherword\n nextword\n andagainword\n" ('\n'- "new line")
  > This console program have 3 modes and escape ("q"):
   *[1]This script can count up and print points of Your words by Scrabble scoring, write '1'
   *[2]response maximal value from file *.txt(default: dictionary.txt) have choosen, write '2'
   *[3]match and response word by value,then write '3'
   -[q]if You want to quit the program then press 'q'
 
 
 #Copyright by owner https://bitbucket.org/Rklab3

