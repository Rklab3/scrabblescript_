import unittest
import run, parentMethods


###
#
NAMEFILE = run.Scrabble(run.Command(), run.File()).filename

run.File().save_word_with_filter(word = "againSAMEWORd", namefile = NAMEFILE)
run.File().save_word_with_filter(word = "SomeworD", namefile = NAMEFILE)
run.File().save_word_with_filter(word = "EAOINRTLSUDGBCMPFHVWYKJXQZ", namefile = NAMEFILE)

class Tests_parent_methods(unittest.TestCase):

    ### TEST EQUAL VALUES
    #
    def type_test_while_of_chars(self):
        expected = 10
        result = parentMethods.ParentMethods().while_through_word_by_chars(word="abcsd")
        self.assertEqual(expected, result)

    ### TEST EQUAL VALUES
    #
    def count_points_test(self):
        expected = 3
        result = parentMethods.ParentMethods().count_points(letterr="b")
        self.assertEqual(expected, result)


class Tests_File(unittest.TestCase):

    ### TEST EQUAL VALUES
    #
    def test_equal_add_scores_to_dict(self):
        dictScore = {}
        value = 11
        key1 = "WORDONE\n"
        key2 = "WORDINN\n"
        dictScore[key1] = value
        dictScore[key2] = value
        result = type(dictScore)
        expected = type({})
        self.assertEqual(expected, result)

    ### TEST EQUAL VALUES
    # if return 0 is not duplicated, if 1 or more is duplicated in txt file
    def test_equal_count_duplicate(self):
        # FILENAME = run.Scrabble(run.Command(), run.File()).filename
        expected = 1
        result = run.File().count_duplicates(word = "SomeworD", namefile = NAMEFILE)
        self.assertEqual(expected, result, "if result = 0 is not duplicated, if result = 1 or more is duplicated in txt file")

    ### TEST EQUAL VALUES
    # Letter capitalization is not necessary
    # If new word first return fail, next is False(because new word in first parameter)
    def test_save_word_with_filter(self):
        # FILENAME = run.Scrabble(run.Command(), run.File()).filename
        expected = False
        result = run.File().save_word_with_filter(word = "againSAMEWORd", namefile = NAMEFILE)
        self.assertEqual(expected, result, "(False == False) return OK")

    def test_save_word(self):
        pass
        # self.assertEqual(run.File())


class Tests_Command(unittest.TestCase):

    def test_equal_get_max_score_from_dict(self):
        dictCollection = {
            'NOW\n': 5,
            'REALLY\n': 9,
            'GREAT\n': 5,
            'FIND\n': 8,
            'AGAIN\n': 5,
            'YES': 5
        }
        result = run.Command().get_max_score_from_dict(dictCollection)
        expected = 9
        self.assertEqual(expected, result)

    def  test_equal_get_name_max_score_from_dict(self):
        dictCollection = {
            'NOW\n': 5,
            'REALLY\n': 9,
            'GREAT\n': 5,
            'FIND\n': 8,
            'AGAIN\n': 5,
            'YES': 5
        }

        result = run.Command().get_name_max_score_from_dict(dictCollection)
        expected = "REALLY\n"
        self.assertEqual(expected, result)

    ### TEST EQUAL VALUES
    # todo to fix
    def test_equal_get_word_by_set_score_from(self):
        setScore = 87
        expected = "EAOINRTLSUDGBCMPFHVWYKJXQZ\n"
        result = run.Command().get_word_by_set_score_from(run.File().add_scores_to_dict(NAMEFILE), setScore)
        self.assertEqual(expected, result)

    ### TEST TYPE
    # todo to fix
    def test_assert_is_types_get_word_by_set_score_from(self):
        expected = type(str())
        result = type(run.Command().get_word_by_set_score_from(run.File().add_scores_to_dict(NAMEFILE), setScore ="4"))
        self.assertIs(expected, result)

    ### TEST EQUAL VALUES
    #
    def test_equal_get_all_by_set_score_from(self):
        value = 11
        fi_word = "WORDONE\n"
        sec_word = "WORDINN\n"
        expected = [(fi_word, value)]
        dictCollection = {fi_word: 11}
        result = run.Command().get_all_by_set_score_from(dictCollection, value)
        self.assertEqual(expected, result)

    ### TEST TYPE
    #
    def test_type_get_all_by_set_score_from(self):

        value = 11
        fi_word = "WORDONE\n"
        sec_word = "WORDINN\n"
        dictCollection = {fi_word: value}
        result = type(run.Command().get_all_by_set_score_from(dictCollection, value))
        expected = type([])
        self.assertEqual(result, expected)



    ### TEST EQUAL VALUES
    # Test that numbers of examples threw between 0 and 25 times are all even equal at least once.
    def test_even_equal_choice_random_from(self):
           # in that test example number of value is no matter
           value = 11
           fi_word = "WORDONE\n"
           sec_word = "WORDINN\n"
           tri_word = "WORDe\n"
           fo_word = "WORD\n"
           resLines = [(fi_word, value), (sec_word, value), (tri_word, value), (fo_word, value), ("WORDo\n", value)]
           i = 0
           liste = []
           while True:
                result = run.Command().choice_random_from(resLines)
                expected = fo_word
                liste.append(bool(expected == result))
                i+=1
                if i == 25:
                    break
           # if contains True, no matter how a lot of, then probably definition choice_random_from() work well done
           if liste.__contains__(True) or liste.__contains__(False) and liste.__contains__(True):
                    if expected == result:
                        self.assertEqual(expected, result)
           elif (not liste.__contains__(True)):
                    self.assertEqual(liste, "if have not any True in list, expected and result are not equal")


if __name__ == '__main__':

    unittest.main()


