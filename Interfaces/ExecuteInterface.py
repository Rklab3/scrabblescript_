from abc import abstractmethod, ABC

class ExecuteInterface(ABC):

    @abstractmethod
    def execute(self): pass

    @abstractmethod
    def test_mode(self): pass
