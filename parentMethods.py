from abc import abstractmethod, ABC
from scoringRules import LETTER_SCORES_1



class ParentMethods(ABC):
    def __init__(self):
        # self.countScore = 0
        pass


    def while_through_word_by_chars(self, word):
        outCharsScore = 0
        for letter in word:
           outCharsScore+= self.count_points(letter)
        return outCharsScore

    # to add up/ to count up the points by the dict()-LETTER_SCORES
    def count_points(self, letterr):
        countScoreLetter = 0
        letterrUp = letterr.upper()
        letterScore = LETTER_SCORES_1
        for key, value in letterScore.items():
            if letterrUp == key:
                countScoreLetter = letterScore[key]
        return countScoreLetter
