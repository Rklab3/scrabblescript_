from colorsb import colors

#Console program main instruction
PROLOGUE = (colors.BLUE+"\n\nHello in Scrabble script: \n"
                               "Mode[1]: This script can count up, save unique word and print points of that word or sentense, please just write '1' in and press <enter>.\n"
                               "Mode[2]: Response maximal value from file *.txt(default: dictionary.txt) have choosen, please just write '2' in and press <enter>.\n"
                               "Mode[3]: Match and response word by value,then please write '3' in and press <enter>.\n"
                               "Escape Mode[q]: If You want to quit the program then press 'q'.\n"+colors.ENDC+""+colors.GREEN+
                               "Please choose by write 1, 2, 3 or q: "+colors.ENDC)

REPEAT = (colors.GREEN+"Try again, please choose 1, 2, 3 or q: "+colors.ENDC)

SCRABBLES_SCORES_1 = [(1, "E A O I N R T L S U"),
                    (2, "D G"),
                    (3, "B C M P"),
                    (4, "F H V W Y"),
                    (5, "K"),
                    (8, "J X"),
                    (10, "Q Z")]

LETTER_SCORES_1 = {letter_1: score_1 for score_1, letters_1 in SCRABBLES_SCORES_1
                 for letter_1 in letters_1.split()}
