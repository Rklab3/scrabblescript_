#!/usr/bin/python

"""
Scrabble Scores:
Classes:
    -ScrabbleExecute: general class to launch each other classes or etc.
    -Dict: create, manage, delete dictionary.txt or ect.
    -Command: take data from console to generate, provide, sum or count data and etc.
    -Logger: 
"""

from Interfaces.ExecuteInterface import ExecuteInterface
from parentMethods import ParentMethods
import operator
from scoringRules import PROLOGUE, REPEAT
from random import choice
from colorsb import colors
import re
import time, datetime
import logging


class Scrabble(ExecuteInterface):


    def __init__(self, command=None, file=None, filename = "dictionary.txt"):
        # super(ExecuteInterface, self).__init__()
        self.command = command
        self.file = file
        self.file.create_file(filename)
        self.file.close_file()
        self.filename = filename
        self.logger = Logger()

    def get_filename(self):
        return self.filename


    def execute(self,  prologue = PROLOGUE):

       try:
           request = self.command.set_input(prologue)

           if request is "1":
                 self.mode_count_up_score()
                 print()
                 self.execute(prologue = REPEAT)
           elif request is "2":
                 self.mode_get_max_value(filename = self.filename)
                 print()
                 self.execute(prologue = REPEAT)
           elif request is "3":
                 self.mode_value_match_word(filename = self.filename)
                 print()
                 self.execute(prologue = REPEAT)
           elif request is "q":
               pass
           else:

               print("\n" + colors.FAIL + "Incorrect command character," + colors.WARNING +
                      " try choose again by CHARACTERS [1], [2], [3] or [q] TO ESCAPE and press <enter>" + colors.ENDC)
               self.execute()

       except Exception as e:
           timee = time.time()
           today = datetime.datetime.now()
           self.logger.initialize_logfile("/../logs/execute.log")
           self.logger.logger_error("Exception: {exception} ! on time = {time}, datetime = {date}"
                                    .format(exception=e, time = timee, date = today))
           # print("Exception: ".format(e))
    ###
    # get word from console, count up score in points and save word in *.txt file
    def mode_count_up_score(self):
        try:
            # word = self.dict.input_word(describe)
            word = str(self.command.set_input(colors.GREEN+"1: Write the TEXT to count it up, save word to file "
                                              +self.filename+", and next press <enter>: "+colors.ENDC))
            response = self.file.save_word_with_filter(word, self.filename)
            if response == False:print("It is duplicate word,We can not save it")
            else: print("Saved word in {}!".format(self.filename))
            print("Score of word-'{}' is: {}".format(word, self.command.while_through_word_by_chars(word)))

        except Exception as e:
            timee = time.time()
            today = datetime.datetime.now()
            self.logger.initialize_logfile("/../logs/mode_count_up_score.log")
            self.logger.logger_error("Exception: {exception} ! on time = {time} : datetime: {date}"
                                     .format(exception=e, time = timee, date = today))
            # print("Exception in mode_count_up_score catched: ".format(e))

    ###
    # get maximum word value from words saved in file *.txt, e.g. dictionary.txt
    def mode_get_max_value(self, filename):
       try:
           dictCollection = self.file.add_scores_to_dict(str(filename))
           print("2: From  file-'{}'".format(filename))
           print(" {} points:  {}".format(self.command.get_max_score_from_dict(dictCollection),
                                          self.command.get_name_max_score_from_dict(dictCollection).upper()))

       except Exception as e:
           timee = time.time()
           today = datetime.datetime.now()
           self.logger.initialize_logfile("/../logs/mode_get_max_value.log")
           self.logger.logger_error("Exception: {exception} ! on time = {time} : datetime: {date}"
                                    .format(exception=e, time = timee, date = today))
           # print("Exception in mode_get_max_value_from catched: {}".format(e))

    ###
    # get the value from console and word form file txt and matched then print out response in console
    def mode_value_match_word(self, filename):
        try:
            value = int(self.command.set_input(colors.GREEN+"3: Search the word by VALUE and press <enter> : "
                                               +colors.ENDC))
            dictCollection = self.file.add_scores_to_dict(filename)
            # responseLine = self.command.get_word_by_set_score_from(dictCollection, value)
            responseLines = self.command.get_all_by_set_score_from(dictCollection, value)
            responseLine = self.command.choice_random_from(responseLines)
            print("From '{}' file:".format(filename))
            print(" Word with value = {} points is:\n {}".format(value, responseLine))

        except Exception as e:
            timee = time.time()
            today = datetime.datetime.now()
            self.logger.initialize_logfile("/../logs/mode_value_match_word.log")
            self.logger.logger_error("Exception: {exception} ! on time = {time} : datetime: {date}"
                                     .format(exception=e, time = timee, date = today))

    def test_mode(self):
        pass
        # print(self.file.add_scores_to_dict("dictionary.txt"))
        # dictCollection = self.file.add_scores_to_dict("dictionary.txt")
        # print(self.command.get_all_by_set_score_from(dictCollection, 4))
        # lc=self.command.get_all_by_set_score_from(dictCollection, 4)
        # print(self.command.choice_random_from(lc))

class Logger():

    def initialize_logfile(self, dirPath):
        # create logger
        PATHROOT = __file__ + dirPath
        logging.basicConfig(filename=PATHROOT, level=logging.DEBUG)
        self.logger = logging.getLogger()

    def logger_error(self, arg):
        return self.logger.error(arg)


    def logger_info(self, arg):
        return self.logger.info(arg)


class File(ParentMethods):

    def __init__(self):
        super().__init__()


    # param: String namefile,
    # create the file if is not exist in path, assign open file to self.variable.
    def create_file(self, namefile):
        self.file = open(namefile, "a+")

    def close_file(self):
       self.file.close()

    def get_file(self):
        return self.file

    # param: String namefile,
    def read_file(self, namefile):
        return open(namefile, "r")

    ###
    # add and count up score to dict() and return it (dictScore)
    def add_scores_to_dict(self, namefile)->{}:
        dictScore = {}
        readfile = self.read_file(namefile)
        with readfile as f:
            for line in f:
                scoreOut = self.while_through_word_by_chars(line)
                key = line
                dictScore[key] = scoreOut
            return dictScore

    ###
    # save word without filter duplicate
    def save_word_to_file(self, word, namefile):
         self.create_file(namefile)
         self.file.write(word+'\n')
         self.file.close()

    ###
    # return int > 0(count up duplicates items) if word is set up and when is contain in items,
    # unless then return int 0
    def count_duplicates(self, word, namefile)->int:
        n = 0

        # upper() for unify word size characters with words from file *.txt
        word = word.upper()

        # wordPattern = re.compile(r'%s' % word)
        wordPattern = re.compile(r'{}'.format(word))
        readFile = self.read_file(namefile)
        with readFile as items:
            for item in items:
                if wordPattern.match(item):
                    n += 1
        return n

    ###
    # if count == 0 then launch save_word_to_file(),
    # if count>0 return False; that filter is for deny saving (word) if duplicates are (namefile)
    def save_word_with_filter(self, word, namefile):
        count = self.count_duplicates(word, namefile)
        if count==0:
            self.save_word_to_file(word.upper(), namefile)
        elif count > 0:
            return False


class Command(ParentMethods):

    def __init__(self):
        super().__init__()

    ###
    # get the input data from console
    def set_input(self, word = str())->str:
        return input(word)

    ###
    # get return from dict the maximum word value
    def get_max_score_from_dict(self, dictCollection):
        points = max(dictCollection.items(), key=operator.itemgetter(1))[1]
        return points

    ###
    # get return the name word from dict with maximal value
    def get_name_max_score_from_dict(self, dictCollection):
        name = max(dictCollection.items(), key=operator.itemgetter(1))[0]
        return name

    ###
    # get return the name word by the value points or return empty string
    # unless setScore == value of some item from dictCollection
    def get_word_by_set_score_from(self, dictCollection, setScore)->str:

        itemsCollection = dictCollection.items()
        setScore = setScore
        for name, value in itemsCollection:
            if setScore == value:
                return name
        return ""

    ###
    # get all words from dict()- dictCollection by value criteria from injection variable-setScore.
    # and get this items to list and return it
    def get_all_by_set_score_from(self, dictCollection, setScore)->[]:
     try:
        listCollection = []
        dictItems = dictCollection.items()
        for searchScores in dictItems:
            if searchScores[1] == setScore:
                listCollection.append(searchScores)
        return listCollection
     except Exception as e:
         print (e)

    ###
    # choice randomly from list()-only one word from many with the same scoring
    def choice_random_from(self, resultList)->str:
        randomChoice = choice(resultList)
        return randomChoice[0]

if __name__ == '__main__':

    Scrabble(Command(), File()).execute()
    # Scrabble(Command(),File()).test_mode()
